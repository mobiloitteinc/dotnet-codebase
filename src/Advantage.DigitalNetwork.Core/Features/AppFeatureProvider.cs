﻿using Abp.Application.Features;
using Abp.Localization;
using Abp.Runtime.Validation;
using Abp.UI.Inputs;

namespace Advantage.DigitalNetwork.Features
{
    public class AppFeatureProvider : FeatureProvider
    {
        public override void SetFeatures(IFeatureDefinitionContext context)
        {
            context.Create(
                AppFeatures.MaxUserCount,
                defaultValue: "0", //0 = unlimited
                displayName: L("MaximumUserCount"),
                description: L("MaximumUserCount_Description"),
                inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
            )[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            {
                ValueTextNormalizer = value => value == "0" ? L("Unlimited") : new FixedLocalizableString(value),
                IsVisibleOnPricingTable = true
            };

            #region Advantage

            /****************************************************
             * Advantage Features
             * 
             ***************************************************/
            var metadata = new FeatureMetadata
            {
                IsVisibleOnPricingTable = true,
                TextHtmlColor = value => value == "true" ? "#5cb85c" : "#d9534f"
            };
            #region MyRegion

            // ********************************************
            // CRM Features
            // ********************************************

            var crmFeature = context.Create(
                AppFeatures.FeatureCrm,
                defaultValue: "false",
                displayName: L("Crm"),
                inputType: new CheckboxInputType()
            );
            crmFeature[FeatureMetadata.CustomFeatureKey] = metadata;

            crmFeature.CreateChildFeature(AppFeatures.FeatureCrmLeads,
                "false",
                L("Leads"),
                L("LeadsDescription"),
                FeatureScopes.All,
                inputType: new CheckboxInputType()
            )[FeatureMetadata.CustomFeatureKey] = metadata;

            crmFeature.CreateChildFeature(AppFeatures.FeatureCrmContacts,
                "false",
                L("Contcats"),
                L("ContactsDescription"),
                FeatureScopes.All,
                inputType: new CheckboxInputType()
            )[FeatureMetadata.CustomFeatureKey] = metadata;

            var fCampaigns = crmFeature.CreateChildFeature(
                AppFeatures.FeatureCrmCampaigns,
                "false",
                L("Campaigns"),
                L("LeadsDescription"),
                FeatureScopes.All,
                inputType: new CheckboxInputType()
            );
            fCampaigns[FeatureMetadata.CustomFeatureKey] = metadata;

            // MassMailing
            var fMailing = crmFeature.CreateChildFeature(
                AppFeatures.FeatureCrmMailing,
                "false",
                L("Mailing"),
                L("MailingDescription"),
                FeatureScopes.All,
                inputType: new CheckboxInputType()
            );
            fMailing[FeatureMetadata.CustomFeatureKey] = metadata;


            fMailing.CreateChildFeature(
                AppFeatures.FeatureCrmMailingMaxMailingPerMonth,
                defaultValue: "1", //0 = unlimited
                displayName: L("Max Mailing Per Month"),
                description: L("MaxMailingPerMonth_Description"),
                inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
            )[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            {
                ValueTextNormalizer = value => value == "0" ? L("Unlimited") : new FixedLocalizableString(value),
                IsVisibleOnPricingTable = true
            };

            fMailing.CreateChildFeature(
                AppFeatures.FeatureCrmMailingMaxEmailsPerMailing,
                defaultValue: "500", //0 = unlimited
                displayName: L("Max Emails Per Mailing"),
                description: L("MaxEmailsPerMailing_Description"),
                inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
            )[FeatureMetadata.CustomFeatureKey] = metadata;


            // Surveys
            var fSurvey = crmFeature.CreateChildFeature(
                AppFeatures.FeatureCrmSurveys,
                "false",
                L("Surveys"),
                L("SurveysDescription"),
                FeatureScopes.All,
                inputType: new CheckboxInputType()
            );
            fSurvey[FeatureMetadata.CustomFeatureKey] = metadata;

            fSurvey.CreateChildFeature(
                    AppFeatures.FeatureCrmSurveysMaxPerMonth,
                    defaultValue: "1", //0 = unlimited
                    displayName: L("Max Serveys Per Month"),
                    description: L("MaxMailingPerMonth_Description"),
                    inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
                )
                [FeatureMetadata.CustomFeatureKey] = metadata;



            // Polls
            var fPolls = crmFeature.CreateChildFeature(
                AppFeatures.FeatureCrmPolls,
                "false",
                L("Polls"),
                L("PollsDescription"),
                FeatureScopes.All,
                inputType: new CheckboxInputType()
            );
            fPolls[FeatureMetadata.CustomFeatureKey] = metadata;

            fPolls.CreateChildFeature(
                    AppFeatures.FeatureCrmPollsMaxPerMonth,
                    defaultValue: "1", //0 = unlimited
                    displayName: L("Max Polls Per Month"),
                    description: L("MaxPollsPerMonth_Description"),
                    inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
                )
                [FeatureMetadata.CustomFeatureKey] = metadata;



            // Social Monitoring
            var fSocialMonitoring = crmFeature.CreateChildFeature(
                AppFeatures.FeatureCrmSocialMontoring,
                "false",
                L("Social Monitoring"),
                L("SocialMonitoringDescription"),
                FeatureScopes.All,
                inputType: new CheckboxInputType()
            )[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            {
                IsVisibleOnPricingTable = true,
                TextHtmlColor = value => value == "true" ? "#5cb85c" : "#d9534f"
            };

            // Social Posting
            var fSocialPosting = crmFeature.CreateChildFeature(
                AppFeatures.FeatureCrmSocialPosting,
                "false",
                L("Social Posting"),
                L("Social Posting Description"),
                FeatureScopes.All,
                inputType: new CheckboxInputType()
            )[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            {
                IsVisibleOnPricingTable = true,
                TextHtmlColor = value => value == "true" ? "#5cb85c" : "#d9534f"
            };







            // ********************************************
            // POS Features
            // ********************************************
            var rmsFeature = context.Create(
                        AppFeatures.FeatureRms,
                        defaultValue: "false",
                        displayName: L("Retail Management"),
                        description: L("Retail Management Description"),
                        inputType: new CheckboxInputType()
                    );
            rmsFeature[FeatureMetadata.CustomFeatureKey] = metadata;
            ;
            rmsFeature.CreateChildFeature(
                    AppFeatures.FeatureRmsStores,
                    defaultValue: "5", //0 = unlimited
                    displayName: L("Max Stores"),
                    description: L("Max_Stores_Description"),
                    inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
                )
                [FeatureMetadata.CustomFeatureKey] = metadata;

            rmsFeature.CreateChildFeature(
                    AppFeatures.FeatureRmsCatalog,
                    defaultValue: "5", //0 = unlimited
                    displayName: L("Max Stores"),
                    description: L("Max_Stores_Description"),
                    inputType: new SingleLineStringInputType(new NumericValueValidator(0, int.MaxValue))
                )
                [FeatureMetadata.CustomFeatureKey] = metadata;

            // ********************************************
            // Loyalty Features
            // ********************************************
            var loyaltyFeature = context.Create(
                AppFeatures.FeatureLoyalty,
                defaultValue: "false",
                displayName: L("Loyalty Management"),
                description: L("Loyalty Management Description"),
                inputType: new CheckboxInputType()
            );
            loyaltyFeature[FeatureMetadata.CustomFeatureKey] = metadata;


            loyaltyFeature.CreateChildFeature(
                    AppFeatures.FeatureLoyaltyAllowMinCustomerClassTarget,
                    defaultValue: "false", //0 = unlimited
                    displayName: L("Allow Min Cust. Class in Program"),
                    description: L("LoyaltyFeatureAllowMinCustomerClassTarget"),
                    inputType: new CheckboxInputType()
                )
                [FeatureMetadata.CustomFeatureKey] = metadata;


            #endregion


            // ********************************************
            // Analytics Features
            // ********************************************
            var analyticsFeature = context.Create(
                AppFeatures.FeatureAnalytics,
                defaultValue: "false",
                displayName: L("Analytics"),
                description: L("Analytics"),
                inputType: new CheckboxInputType()
            );
            analyticsFeature[FeatureMetadata.CustomFeatureKey] = metadata;


            // ********************************************
            // eCommerce Features
            // ********************************************
            var eCommerceFeature = context.Create(
                AppFeatures.FeatureECommerce,
                defaultValue: "false",
                displayName: L("ECommerce"),
                description: L("ECommerce"),
                inputType: new CheckboxInputType()
            );
            eCommerceFeature[FeatureMetadata.CustomFeatureKey] = metadata;



            /****************************************************
             * End Advantage Features
             * 
             ***************************************************/


            #endregion

            #region ######## Example Features - You can delete them #########

            context.Create(
                AppFeatures.TestCheckFeature,
                defaultValue: "false",
                displayName: L("TestCheckFeature"),
                inputType: new CheckboxInputType()
            )[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            {
                IsVisibleOnPricingTable = true,
                TextHtmlColor = value => value == "true" ? "#5cb85c" : "#d9534f"
            };

            context.Create(
                AppFeatures.TestCheckFeature2,
                defaultValue: "true",
                displayName: L("TestCheckFeature2"),
                inputType: new CheckboxInputType()
            )[FeatureMetadata.CustomFeatureKey] = new FeatureMetadata
            {
                IsVisibleOnPricingTable = true,
                TextHtmlColor = value => value == "true" ? "#5cb85c" : "#d9534f"
            };

            #endregion

#if FEATURE_SIGNALR

            var chatFeature = context.Create(
                AppFeatures.ChatFeature,
                defaultValue: "false",
                displayName: L("ChatFeature"),
                inputType: new CheckboxInputType()
            );

            chatFeature.CreateChildFeature(
                AppFeatures.TenantToTenantChatFeature,
                defaultValue: "false",
                displayName: L("TenantToTenantChatFeature"),
                inputType: new CheckboxInputType()
            );

            chatFeature.CreateChildFeature(
                AppFeatures.TenantToHostChatFeature,
                defaultValue: "false",
                displayName: L("TenantToHostChatFeature"),
                inputType: new CheckboxInputType()
            );

#endif
        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, DigitalNetworkConsts.LocalizationSourceName);
        }
    }
}
