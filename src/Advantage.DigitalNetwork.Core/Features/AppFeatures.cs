﻿namespace Advantage.DigitalNetwork.Features
{
    public static class AppFeatures
    {
        public const string MaxUserCount = "App.MaxUserCount";
        public const string ChatFeature = "App.ChatFeature";
        public const string TenantToTenantChatFeature = "App.ChatFeature.TenantToTenant";
        public const string TenantToHostChatFeature = "App.ChatFeature.TenantToHost";
        public const string TestCheckFeature = "App.TestCheckFeature";
        public const string TestCheckFeature2 = "App.TestCheckFeature2";


        // CRM
        public const string FeatureCrm = "App.CrmFeature";
        public const string FeatureCrmContacts = "App.CrmFeature.Contacts";
        public const string FeatureCrmLeads = "App.CrmFeature.Leads";
        public const string FeatureCrmCampaigns = "App.CrmFeature.Campaigns";
        public const string FeatureCrmMailing = "App.CrmFeature.Mailing";
        public const string FeatureCrmMailingMaxMailingPerMonth = "App.CrmFeature.Mailing.MaxPerMailingMonth";
        public const string FeatureCrmMailingMaxEmailsPerMailing = "App.CrmFeature.Mailing.MaxEmailsPerMailing";


        public const string FeatureCrmSurveys = "App.CrmFeature.Surveys";
        public const string FeatureCrmSurveysMaxPerMonth = "App.CrmFeature.Surveys.MaxPerMonth";


        public const string FeatureCrmPolls = "App.CrmFeature.Polls";
        public const string FeatureCrmPollsMaxPerMonth = "App.CrmFeature.Polls.MaxPerMonth";

        public const string FeatureCrmSocialMontoring = "App.CrmFeature.SocialMontoring";
        public const string FeatureCrmSocialPosting = "App.CrmFeature.SocialPosting";

        // Analytics
        public const string FeatureAnalytics = "App.AnalyticsFeature";

        // Loyalty
        public const string FeatureLoyalty = "App.LoyaltyFeature";
        public const string FeatureLoyaltyAllowMinCustomerClassTarget = "App.LoyaltyFeature.AllowMinCustomerClassTarget";


        //RMS
        public const string FeatureRms = "App.RMSFeature";
        public const string FeatureRmsStores = "App.RMSFeature.Stores";
        public const string FeatureRmsStoresMaxStores = "App.RMSFeature.Stores.MaxStores";

        public const string FeatureRmsCatalog = "App.RMSFeature.Catalog";
        public const string FeatureRmsCatalogCategories = "App.RMSFeature.Catalog.Categories";
        public const string FeatureRmsCatalogProducts = "App.RMSFeature.Catalog.Products";


        //eCommerce
        public const string FeatureECommerce = "App.ECommerceFeature";


    }
}