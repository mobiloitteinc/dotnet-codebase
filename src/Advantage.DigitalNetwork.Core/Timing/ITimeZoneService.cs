﻿using System.Threading.Tasks;
using Abp.Configuration;

namespace Advantage.DigitalNetwork.Timing
{
    public interface ITimeZoneService
    {
        Task<string> GetDefaultTimezoneAsync(SettingScopes scope, int? tenantId);
    }
}
