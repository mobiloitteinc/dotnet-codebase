﻿namespace Advantage.DigitalNetwork.Authorization
{
    /// <summary>
    /// Defines string constants for application's permission names.
    /// <see cref="AppAuthorizationProvider"/> for permission definitions.
    /// </summary>
    public static class AppPermissions
    {
        //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

        public const string Pages = "Pages";

        public const string Pages_DemoUiComponents= "Pages.DemoUiComponents";
        public const string Pages_Administration = "Pages.Administration";

        public const string Pages_Administration_Roles = "Pages.Administration.Roles";
        public const string Pages_Administration_Roles_Create = "Pages.Administration.Roles.Create";
        public const string Pages_Administration_Roles_Edit = "Pages.Administration.Roles.Edit";
        public const string Pages_Administration_Roles_Delete = "Pages.Administration.Roles.Delete";

        public const string Pages_Administration_Users = "Pages.Administration.Users";
        public const string Pages_Administration_Users_Create = "Pages.Administration.Users.Create";
        public const string Pages_Administration_Users_Edit = "Pages.Administration.Users.Edit";
        public const string Pages_Administration_Users_Delete = "Pages.Administration.Users.Delete";
        public const string Pages_Administration_Users_ChangePermissions = "Pages.Administration.Users.ChangePermissions";
        public const string Pages_Administration_Users_Impersonation = "Pages.Administration.Users.Impersonation";

        public const string Pages_Administration_Languages = "Pages.Administration.Languages";
        public const string Pages_Administration_Languages_Create = "Pages.Administration.Languages.Create";
        public const string Pages_Administration_Languages_Edit = "Pages.Administration.Languages.Edit";
        public const string Pages_Administration_Languages_Delete = "Pages.Administration.Languages.Delete";
        public const string Pages_Administration_Languages_ChangeTexts = "Pages.Administration.Languages.ChangeTexts";

        public const string Pages_Administration_AuditLogs = "Pages.Administration.AuditLogs";

        public const string Pages_Administration_OrganizationUnits = "Pages.Administration.OrganizationUnits";
        public const string Pages_Administration_OrganizationUnits_ManageOrganizationTree = "Pages.Administration.OrganizationUnits.ManageOrganizationTree";
        public const string Pages_Administration_OrganizationUnits_ManageMembers = "Pages.Administration.OrganizationUnits.ManageMembers";

        public const string Pages_Administration_HangfireDashboard = "Pages.Administration.HangfireDashboard";

        public const string Pages_Administration_UiCustomization = "Pages.Administration.UiCustomization";

        //TENANT-SPECIFIC PERMISSIONS

        public const string Pages_Tenant_Dashboard = "Pages.Tenant.Dashboard";

        public const string Pages_Administration_Tenant_Settings = "Pages.Administration.Tenant.Settings";

        public const string Pages_Administration_Tenant_SubscriptionManagement = "Pages.Administration.Tenant.SubscriptionManagement";

        //HOST-SPECIFIC PERMISSIONS

        public const string Pages_Editions = "Pages.Editions";
        public const string Pages_Editions_Create = "Pages.Editions.Create";
        public const string Pages_Editions_Edit = "Pages.Editions.Edit";
        public const string Pages_Editions_Delete = "Pages.Editions.Delete";

        public const string Pages_Tenants = "Pages.Tenants";
        public const string Pages_Tenants_Create = "Pages.Tenants.Create";
        public const string Pages_Tenants_Edit = "Pages.Tenants.Edit";
        public const string Pages_Tenants_ChangeFeatures = "Pages.Tenants.ChangeFeatures";
        public const string Pages_Tenants_Delete = "Pages.Tenants.Delete";
        public const string Pages_Tenants_Impersonation = "Pages.Tenants.Impersonation";

        public const string Pages_Administration_Host_Maintenance = "Pages.Administration.Host.Maintenance";
        public const string Pages_Administration_Host_Settings = "Pages.Administration.Host.Settings";
        public const string Pages_Administration_Host_Dashboard = "Pages.Administration.Host.Dashboard";


        // RMS Permissions
        public const string Rms = "Rms";
        public const string Rms_Stores = "Rms.Stores";
        public const string Rms_Stores_Create = "Rms.Stores.Create";
        public const string Rms_Stores_Edit = "Rms.Stores.Edit";
        public const string Rms_Stores_Delete = "Rms.Stores.Delete";

        public const string Rms_Catalog = "Rms.Catalog";
        public const string Rms_Catalog_Products_Create = "Rms.Catalog.Products.Create";
        public const string Rms_Catalog_Products_Edit = "Rms.Catalog.Products.Edit";
        public const string Rms_Catalog_Products_Delete = "Rms.Catalog.Products.Delete";

        public const string Rms_Catalog_Categories_Create = "Rms.Catalog.Categories.Create";
        public const string Rms_Catalog_Categories_Edit = "Rms.Catalog.Categories.Edit";
        public const string Rms_Catalog_Categories_Delete = "Rms.Catalog.Categories.Delete";

        // CRM Permissions
        public const string Pages_Crm = "Crm";
        public const string Crm_Contacts = "Crm.Contacts";
        public const string Crm_Contacts_Create = "Crm.Contacts.Create";
        public const string Crm_Contacts_Edit = "Crm.Contacts.Edit";
        public const string Crm_Contacts_Delete = "Crm.Contacts.Delete";



        // Loyalty Permissions
        public const string Loyalty = "Loyalty";
        public const string Loyalty_Settings = "Loyalty.Settings";

        //public const string Crm_Contacts = "Crm.Contacts";


        // Anaytics Permissions
        public const string Anaytics = "Anaytics";

        // Loyalty Permissions
        public const string ECommerce = "ECommerce";

        //Tax Rate Permission
        public const string Pages_Administration_TaxRate = "Pages.Administration.TaxRate";
        public const string Pages_Administration_TaxRate_Create = "Pages.Administration.TaxRate.Create";
        public const string Pages_Administration_TaxRate_Edit = "Pages.Administration.TaxRate.Edit";
        public const string Pages_Administration_TaxRate_Delete = "Pages.Administration.TaxRate.Delete";
        //Tax Category Permission
        public const string Pages_Administration_TaxCategory = "Pages.Administration.TaxCategory";
        public const string Pages_Administration_TaxCategory_Create = "Pages.Administration.TaxCategory.Create";
        public const string Pages_Administration_TaxCategory_Edit = "Pages.Administration.TaxCategory.Edit";
        public const string Pages_Administration_TaxCategory_Delete = "Pages.Administration.TaxCategory.Delete";
        //Country Permission
        public const string Pages_Administration_Country = "Pages.Administration.Country";
        public const string Pages_Administration_Country_Create = "Pages.Administration.Country.Create";
        public const string Pages_Administration_Country_Edit = "Pages.Administration.Country.Edit";
        public const string Pages_Administration_Country_Delete = "Pages.Administration.Country.Delete";
        //Currency Permission
        public const string Pages_Administration_Currency = "Pages.Administration.Currency";
        public const string Pages_Administration_Currency_Create = "Pages.Administration.Currency.Create";
        public const string Pages_Administration_Currency_Edit = "Pages.Administration.Currency.Edit";
        public const string Pages_Administration_Currency_Delete = "Pages.Administration.Currency.Delete";
        //Tax Settings Permission
        public const string Pages_Administration_TaxSetting = "Pages.Administration.TaxSetting";
        public const string Pages_Administration_TaxSetting_Create = "Pages.Administration.TaxSetting.Create";
        public const string Pages_Administration_TaxSetting_Edit = "Pages.Administration.TaxSetting.Edit";
        public const string Pages_Administration_TaxSetting_Delete = "Pages.Administration.TaxSetting.Delete";
    }
}