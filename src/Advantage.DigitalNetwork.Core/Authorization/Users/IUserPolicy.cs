﻿using System.Threading.Tasks;
using Abp.Domain.Policies;

namespace Advantage.DigitalNetwork.Authorization.Users
{
    public interface IUserPolicy : IPolicy
    {
        Task CheckMaxUserCountAsync(int tenantId);
    }
}
