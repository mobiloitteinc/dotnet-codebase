﻿using Abp.Authorization;
using Abp.Configuration.Startup;
using Abp.Localization;
using Abp.MultiTenancy;

namespace Advantage.DigitalNetwork.Authorization
{
    /// <summary>
    /// Application's authorization provider.
    /// Defines permissions for the application.
    /// See <see cref="AppPermissions"/> for all permission names.
    /// </summary>
    public class AppAuthorizationProvider : AuthorizationProvider
    {
        private readonly bool _isMultiTenancyEnabled;

        public AppAuthorizationProvider(bool isMultiTenancyEnabled)
        {
            _isMultiTenancyEnabled = isMultiTenancyEnabled;
        }

        public AppAuthorizationProvider(IMultiTenancyConfig multiTenancyConfig)
        {
            _isMultiTenancyEnabled = multiTenancyConfig.IsEnabled;
        }

        public override void SetPermissions(IPermissionDefinitionContext context)
        {
            //COMMON PERMISSIONS (FOR BOTH OF TENANTS AND HOST)

            var pages = context.GetPermissionOrNull(AppPermissions.Pages) ?? context.CreatePermission(AppPermissions.Pages, L("Pages"));
            pages.CreateChildPermission(AppPermissions.Pages_DemoUiComponents, L("DemoUiComponents"));

            var administration = pages.CreateChildPermission(AppPermissions.Pages_Administration, L("Administration"));

            var roles = administration.CreateChildPermission(AppPermissions.Pages_Administration_Roles, L("Roles"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Create, L("CreatingNewRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Edit, L("EditingRole"));
            roles.CreateChildPermission(AppPermissions.Pages_Administration_Roles_Delete, L("DeletingRole"));

            var users = administration.CreateChildPermission(AppPermissions.Pages_Administration_Users, L("Users"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Create, L("CreatingNewUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Edit, L("EditingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Delete, L("DeletingUser"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_ChangePermissions, L("ChangingPermissions"));
            users.CreateChildPermission(AppPermissions.Pages_Administration_Users_Impersonation, L("LoginForUsers"));

            var languages = administration.CreateChildPermission(AppPermissions.Pages_Administration_Languages, L("Languages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Create, L("CreatingNewLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Edit, L("EditingLanguage"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_Delete, L("DeletingLanguages"));
            languages.CreateChildPermission(AppPermissions.Pages_Administration_Languages_ChangeTexts, L("ChangingTexts"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_AuditLogs, L("AuditLogs"));

            var organizationUnits = administration.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits, L("OrganizationUnits"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageOrganizationTree, L("ManagingOrganizationTree"));
            organizationUnits.CreateChildPermission(AppPermissions.Pages_Administration_OrganizationUnits_ManageMembers, L("ManagingMembers"));

            administration.CreateChildPermission(AppPermissions.Pages_Administration_UiCustomization, L("VisualSettings"));

            var taxRates = administration.CreateChildPermission(AppPermissions.Pages_Administration_TaxRate, L("TaxRate"));
            taxRates.CreateChildPermission(AppPermissions.Pages_Administration_TaxRate_Create, L("CreatingNewTaxRate"));
            taxRates.CreateChildPermission(AppPermissions.Pages_Administration_TaxRate_Edit, L("EditingTaxRate"));
            taxRates.CreateChildPermission(AppPermissions.Pages_Administration_TaxRate_Delete, L("DeletingTaxRate"));

            var Countries = administration.CreateChildPermission(AppPermissions.Pages_Administration_Country, L("Country"));
            Countries.CreateChildPermission(AppPermissions.Pages_Administration_Country_Create, L("CreatingNewCountry"));
            Countries.CreateChildPermission(AppPermissions.Pages_Administration_Country_Edit, L("EditingCountry"));
            Countries.CreateChildPermission(AppPermissions.Pages_Administration_Country_Delete, L("DeletingCountry"));

            var Currencies = administration.CreateChildPermission(AppPermissions.Pages_Administration_Currency, L("Currency"));
            Currencies.CreateChildPermission(AppPermissions.Pages_Administration_Currency_Create, L("CreatingNewCurrency"));
            Currencies.CreateChildPermission(AppPermissions.Pages_Administration_Currency_Edit, L("EditingCurrency"));
            Currencies.CreateChildPermission(AppPermissions.Pages_Administration_Currency_Delete, L("DeletingCurrency"));

            var taxCategory = administration.CreateChildPermission(AppPermissions.Pages_Administration_TaxCategory, L("TaxCategory"));
            taxCategory.CreateChildPermission(AppPermissions.Pages_Administration_TaxCategory_Create, L("CreatingNewTaxCategory"));
            taxCategory.CreateChildPermission(AppPermissions.Pages_Administration_TaxCategory_Edit, L("EditingTaxCategory"));
            taxCategory.CreateChildPermission(AppPermissions.Pages_Administration_TaxCategory_Delete, L("DeletingTaxCategory"));



            //TENANT-SPECIFIC PERMISSIONS

            pages.CreateChildPermission(AppPermissions.Pages_Tenant_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Tenant);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Tenant_SubscriptionManagement, L("Subscription"), multiTenancySides: MultiTenancySides.Tenant);

            //HOST-SPECIFIC PERMISSIONS

            var editions = pages.CreateChildPermission(AppPermissions.Pages_Editions, L("Editions"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Create, L("CreatingNewEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Edit, L("EditingEdition"), multiTenancySides: MultiTenancySides.Host);
            editions.CreateChildPermission(AppPermissions.Pages_Editions_Delete, L("DeletingEdition"), multiTenancySides: MultiTenancySides.Host);

            var tenants = pages.CreateChildPermission(AppPermissions.Pages_Tenants, L("Tenants"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Create, L("CreatingNewTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Edit, L("EditingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_ChangeFeatures, L("ChangingFeatures"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Delete, L("DeletingTenant"), multiTenancySides: MultiTenancySides.Host);
            tenants.CreateChildPermission(AppPermissions.Pages_Tenants_Impersonation, L("LoginForTenants"), multiTenancySides: MultiTenancySides.Host);

            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Settings, L("Settings"), multiTenancySides: MultiTenancySides.Host);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Maintenance, L("Maintenance"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_HangfireDashboard, L("HangfireDashboard"), multiTenancySides: _isMultiTenancyEnabled ? MultiTenancySides.Host : MultiTenancySides.Tenant);
            administration.CreateChildPermission(AppPermissions.Pages_Administration_Host_Dashboard, L("Dashboard"), multiTenancySides: MultiTenancySides.Host);


            // RMS Permissions
            var rms = pages.CreateChildPermission(AppPermissions.Rms, L("Rms"));

            var stores = rms.CreateChildPermission(AppPermissions.Rms_Stores, L("Stores"), multiTenancySides: MultiTenancySides.Tenant | MultiTenancySides.Host);
            stores.CreateChildPermission(AppPermissions.Rms_Stores_Create, L("CreateStore"));
            stores.CreateChildPermission(AppPermissions.Rms_Stores_Edit, L("EditStore"));
            stores.CreateChildPermission(AppPermissions.Rms_Stores_Delete, L("DeleteStore"));

            var catalog = rms.CreateChildPermission(AppPermissions.Rms_Catalog, L("Catalog"));
            catalog.CreateChildPermission(AppPermissions.Rms_Catalog_Products_Create, L("CreateProduct"));
            catalog.CreateChildPermission(AppPermissions.Rms_Catalog_Products_Edit, L("EditProduct"));
            catalog.CreateChildPermission(AppPermissions.Rms_Catalog_Products_Delete, L("DeleteProduct"));



            // CRM Permissions
            var crm = pages.CreateChildPermission(AppPermissions.Pages_Crm, L("Crm"));

            var contacts = crm.CreateChildPermission(AppPermissions.Crm_Contacts, L("Contacts"));
            contacts.CreateChildPermission(AppPermissions.Crm_Contacts_Create, L("CreateContact"));
            contacts.CreateChildPermission(AppPermissions.Crm_Contacts_Edit, L("EditContact"));
            contacts.CreateChildPermission(AppPermissions.Crm_Contacts_Delete, L("DeleteContact"));


            // Loyalty Permissions
            var loyalty = pages.CreateChildPermission(AppPermissions.Loyalty, L("Loyalty"));


            // Analytics Permissions
               var analytics = pages.CreateChildPermission(AppPermissions.Anaytics, L("Analytics"));

            // eCommerce Permissions
             var eCommerce = pages.CreateChildPermission(AppPermissions.ECommerce, L("ECommerce"));

        }

        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, DigitalNetworkConsts.LocalizationSourceName);
        }
    }
}
