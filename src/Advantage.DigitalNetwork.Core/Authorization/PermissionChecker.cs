﻿using Abp.Authorization;
using Advantage.DigitalNetwork.Authorization.Roles;
using Advantage.DigitalNetwork.Authorization.Users;

namespace Advantage.DigitalNetwork.Authorization
{
    public class PermissionChecker : PermissionChecker<Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
