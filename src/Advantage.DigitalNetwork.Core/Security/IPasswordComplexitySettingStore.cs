﻿using System.Threading.Tasks;

namespace Advantage.DigitalNetwork.Security
{
    public interface IPasswordComplexitySettingStore
    {
        Task<PasswordComplexitySetting> GetSettingsAsync();
    }
}
