using Newtonsoft.Json;

namespace Advantage.DigitalNetwork.MultiTenancy.Payments.Paypal
{
    public class Payer
    {
        [JsonProperty("payment_method")]
        public string PaymentMethod { get; set; }
    }
}