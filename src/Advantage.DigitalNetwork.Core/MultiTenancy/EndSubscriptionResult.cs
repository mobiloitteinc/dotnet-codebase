namespace Advantage.DigitalNetwork.MultiTenancy
{
    public enum EndSubscriptionResult
    {
        TenantSetInActive,
        AssignedToAnotherEdition
    }
}