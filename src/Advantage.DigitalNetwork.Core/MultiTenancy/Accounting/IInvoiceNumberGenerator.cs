﻿using System.Threading.Tasks;
using Abp.Dependency;

namespace Advantage.DigitalNetwork.MultiTenancy.Accounting
{
    public interface IInvoiceNumberGenerator : ITransientDependency
    {
        Task<string> GetNewInvoiceNumber();
    }
}