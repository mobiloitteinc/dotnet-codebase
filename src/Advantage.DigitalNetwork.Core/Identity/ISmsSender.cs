using System.Threading.Tasks;

namespace Advantage.DigitalNetwork.Identity
{
    public interface ISmsSender
    {
        Task SendAsync(string number, string message);
    }
}