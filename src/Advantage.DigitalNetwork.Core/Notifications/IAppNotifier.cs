﻿using System.Threading.Tasks;
using Abp;
using Abp.Notifications;
using Advantage.DigitalNetwork.Authorization.Users;
using Advantage.DigitalNetwork.MultiTenancy;

namespace Advantage.DigitalNetwork.Notifications
{
    public interface IAppNotifier
    {
        Task WelcomeToTheApplicationAsync(User user);

        Task NewUserRegisteredAsync(User user);

        Task NewTenantRegisteredAsync(Tenant tenant);

        Task SendMessageAsync(UserIdentifier user, string message, NotificationSeverity severity = NotificationSeverity.Info);
    }
}
