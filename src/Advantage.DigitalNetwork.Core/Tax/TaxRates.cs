﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Advantage.DigitalNetwork.Tax
{
    [Table("AbpTaxRate")]
    public class TaxRates : Entity<int>, IHasCreationTime
    {
        public int StoreId { get; set; }
        public int TaxCategoryId { get; set; }
        public int CountryId { get; set; }
        public int StateProvinceId { get; set; }
        public string Zip { get; set; }
        public decimal Percentage { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
