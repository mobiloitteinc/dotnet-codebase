﻿using Abp.Domain.Entities;
using Abp.Domain.Entities.Auditing;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Advantage.DigitalNetwork.Tax
{
    [Table("AbpTaxCategory")]
    public class TaxCategory : Entity<int>, IHasCreationTime
    {
        public string Name { get; set; }
        public int DisplayOrder { get; set; }
        public DateTime CreationTime { get; set; }
    }

}
