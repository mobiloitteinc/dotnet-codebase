﻿using Microsoft.Extensions.Configuration;

namespace Advantage.DigitalNetwork.Configuration
{
    public interface IAppConfigurationAccessor
    {
        IConfigurationRoot Configuration { get; }
    }
}
