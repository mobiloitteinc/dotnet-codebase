﻿using Abp.Domain.Services;

namespace Advantage.DigitalNetwork
{
    public abstract class DigitalNetworkDomainServiceBase : DomainService
    {
        /* Add your common members for all your domain services. */

        protected DigitalNetworkDomainServiceBase()
        {
            LocalizationSourceName = DigitalNetworkConsts.LocalizationSourceName;
        }
    }
}
