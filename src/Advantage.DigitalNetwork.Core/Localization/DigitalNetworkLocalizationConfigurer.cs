﻿using System.Reflection;
using Abp.Configuration.Startup;
using Abp.Localization.Dictionaries;
using Abp.Localization.Dictionaries.Xml;
using Abp.Reflection.Extensions;

namespace Advantage.DigitalNetwork.Localization
{
    public static class DigitalNetworkLocalizationConfigurer
    {
        public static void Configure(ILocalizationConfiguration localizationConfiguration)
        {
            localizationConfiguration.Sources.Add(
                new DictionaryBasedLocalizationSource(
                    DigitalNetworkConsts.LocalizationSourceName,
                    new XmlEmbeddedFileLocalizationDictionaryProvider(
                        typeof(DigitalNetworkLocalizationConfigurer).GetAssembly(),
                        "Advantage.DigitalNetwork.Localization.DigitalNetwork"
                    )
                )
            );
        }
    }
}