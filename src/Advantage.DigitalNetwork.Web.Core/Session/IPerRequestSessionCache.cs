﻿using System.Threading.Tasks;
using Advantage.DigitalNetwork.Sessions.Dto;

namespace Advantage.DigitalNetwork.Web.Session
{
    public interface IPerRequestSessionCache
    {
        Task<GetCurrentLoginInformationsOutput> GetCurrentLoginInformationsAsync();
    }
}
