﻿namespace Advantage.DigitalNetwork
{
    public class DigitalNetworkConsts
    {
        public const string LocalizationSourceName = "DigitalNetwork";

        public const string ConnectionStringName = "Default";

        public const bool MultiTenancyEnabled = true;

        public const int PaymentCacheDurationInMinutes = 30;
    }
}