﻿namespace Advantage.DigitalNetwork.Chat
{
    public enum ChatSide
    {
        Sender = 1,

        Receiver = 2
    }
}