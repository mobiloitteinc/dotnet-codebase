namespace Advantage.DigitalNetwork.MultiTenancy.Payments.Dto
{
    public class GetSubscriptionPaymentInput
    {
        public long Id { get; set; }
    }
}