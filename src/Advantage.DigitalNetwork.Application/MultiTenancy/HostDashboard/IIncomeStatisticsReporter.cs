﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Advantage.DigitalNetwork.MultiTenancy.HostDashboard.Dto;

namespace Advantage.DigitalNetwork.MultiTenancy.HostDashboard
{
    public interface IIncomeStatisticsService
    {
        Task<List<IncomeStastistic>> GetIncomeStatisticsData(DateTime startDate, DateTime endDate,
            ChartDateInterval dateInterval);
    }
}