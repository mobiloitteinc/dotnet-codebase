﻿using System.Threading.Tasks;
using Abp.Application.Services;

namespace Advantage.DigitalNetwork.MultiTenancy
{
    public interface ISubscriptionAppService : IApplicationService
    {
        Task UpgradeTenantToEquivalentEdition(int upgradeEditionId);
    }
}
