﻿using System.Threading.Tasks;
using Abp.Application.Services.Dto;
using Advantage.DigitalNetwork.MultiTenancy.Accounting.Dto;

namespace Advantage.DigitalNetwork.MultiTenancy.Accounting
{
    public interface IInvoiceAppService
    {
        Task<InvoiceDto> GetInvoiceInfo(EntityDto<long> input);

        Task CreateInvoice(CreateInvoiceDto input);
    }
}
