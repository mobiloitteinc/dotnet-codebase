namespace Advantage.DigitalNetwork.MultiTenancy.Accounting.Dto
{
    public class CreateInvoiceDto
    {
        public long SubscriptionPaymentId { get; set; }
    }
}