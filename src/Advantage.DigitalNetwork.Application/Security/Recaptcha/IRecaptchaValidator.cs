using System.Threading.Tasks;

namespace Advantage.DigitalNetwork.Security.Recaptcha
{
    public interface IRecaptchaValidator
    {
        Task ValidateAsync(string captchaResponse);
    }
}