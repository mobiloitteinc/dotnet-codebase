﻿using System.Collections.Generic;
using System.Linq;
using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Advantage.DigitalNetwork.Currency.Dto;

namespace Advantage.DigitalNetwork.Currency
{
    public class CurrencyService : DigitalNetworkAppServiceBase, ICurrencyService
    {
        private readonly IRepository<Currency, int> _currencyRepository;

        public CurrencyService(IRepository<Currency, int> currencyRepository)
        {
            _currencyRepository = currencyRepository;
        }

        public ListResultDto<CurrencyDto> GetCurrencies()
        {
            var currency = _currencyRepository.GetAllIncluding();
            return new ListResultDto<CurrencyDto>(ObjectMapper.Map<List<CurrencyDto>>(currency));
        }
        //public List<CurrencyDto> GetCurrencies()
        //{
        //    var messages = _currencyRepository.GetAllList()
        //         .Take(50)
        //         .ToList();
        //    messages.Reverse();
        //    return new List<CurrencyDto>(ObjectMapper.Map<List<CurrencyDto>>(messages));
        //}

        //ListResultDto<CurrencyDto> ICurrencyService.GetCurrencies()
        //{
        //    throw new System.NotImplementedException();
        //}
    }
}
