﻿using System.Collections.Generic;
using Advantage.DigitalNetwork.Auditing.Dto;
using Advantage.DigitalNetwork.Dto;

namespace Advantage.DigitalNetwork.Auditing.Exporting
{
    public interface IAuditLogListExcelExporter
    {
        FileDto ExportToFile(List<AuditLogListDto> auditLogListDtos);
    }
}
