﻿namespace Advantage.DigitalNetwork.Auditing
{
    public interface INamespaceStripper
    {
        string StripNameSpace(string serviceName);
    }
}