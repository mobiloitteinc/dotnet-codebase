﻿using System.Threading.Tasks;
using Abp.Application.Services;
using Advantage.DigitalNetwork.Install.Dto;

namespace Advantage.DigitalNetwork.Install
{
    public interface IInstallAppService : IApplicationService
    {
        Task Setup(InstallDto input);

        AppSettingsJsonDto GetAppSettingsJson();

        CheckDatabaseOutput CheckDatabase();
    }
}