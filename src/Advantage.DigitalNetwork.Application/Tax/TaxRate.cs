﻿using System;
using System.Collections.Generic;
using System.Text;
using Advantage.DigitalNetwork.Tax.Dto;
using Abp.Authorization;
using Abp.Domain.Repositories;
using System.Linq;
using Abp.Application.Services.Dto;

namespace Advantage.DigitalNetwork.Tax
{
    [AbpAuthorize]
    public class TaxRate : DigitalNetworkAppServiceBase, ITaxRate
    {
        private readonly IRepository<TaxRates, int> _taxRateCategoryRepository;
        public TaxRate(IRepository<TaxRates, int> TaxRateCategoryRepository)
        {
            _taxRateCategoryRepository = TaxRateCategoryRepository;
        }

        public ListResultDto<TaxDto> GetTaxRate()
        {
            var result = _taxRateCategoryRepository.GetAll();
            return new ListResultDto<TaxDto>(ObjectMapper.Map<List<TaxDto>>(result));
        }
    }
}
