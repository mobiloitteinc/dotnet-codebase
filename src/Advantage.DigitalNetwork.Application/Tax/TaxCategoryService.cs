﻿using Abp.Application.Services.Dto;
using Abp.Domain.Repositories;
using Advantage.DigitalNetwork.Tax.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Advantage.DigitalNetwork.Tax
{
    public class TaxCategoryService : DigitalNetworkAppServiceBase, ITaxService
    {
        private readonly IRepository<TaxCategory, int> _taxCategoryRepository;
        public TaxCategoryService(IRepository<TaxCategory, int> TaxCategoryRepository)
        {
            _taxCategoryRepository = TaxCategoryRepository;
        }
        public ListResultDto<TaxCategoryDto> GetTaxCategories()
        {
            var result = _taxCategoryRepository.GetAll();
                 
            return new ListResultDto<TaxCategoryDto>(ObjectMapper.Map<List<TaxCategoryDto>>(result));
        }

        //public Task<TaxCategoryOutputModel> CreateTaxCategory(TaxCategoryDto model)
        //{

        //}
    }
}
