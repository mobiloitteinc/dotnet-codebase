﻿using Advantage.DigitalNetwork.Countries;
using System;
using System.Collections.Generic;
using System.Text;
using Advantage.DigitalNetwork.Countries.Dto;
using System.Threading.Tasks;
using Abp.Authorization;
using Abp.Domain.Repositories;
using System.Linq;
using Abp.Application.Services.Dto;
using Microsoft.EntityFrameworkCore;
using Abp.Collections.Extensions;
using Advantage.DigitalNetwork.EntityFrameworkCore;

namespace Advantage.DigitalNetwork.Country
{
    [AbpAuthorize]
    public class CountryService : DigitalNetworkAppServiceBase, ICountryService
    {
        private readonly IRepository<Country, int> _countryRepository;


        public CountryService(IRepository<Country, int> countryRepository)
        {
            _countryRepository = countryRepository;
        }

        public Task<CountryDto> CreateCountry(CountryRequestInput input)
        {
            throw new NotImplementedException();
        }

        public Task DeleteCountry(int Id)
        {
            throw new NotImplementedException();
        }

        public ListResultDto<CountryDto> GetCountries()
        {
            var country = _countryRepository.GetAll();
            return new ListResultDto<CountryDto>(ObjectMapper.Map<List<CountryDto>>(country));
        }

        public ListResultDto<CountryDto> GetCountriesToExcel()
        {
            var country = _countryRepository.GetAllIncluding();
            return new ListResultDto<CountryDto>(ObjectMapper.Map<List<CountryDto>>(country));
        }

        public Task<CountryDto> UpdateCountry(CountryRequestInput input)
        {
            throw new NotImplementedException();
        }
    }
}
