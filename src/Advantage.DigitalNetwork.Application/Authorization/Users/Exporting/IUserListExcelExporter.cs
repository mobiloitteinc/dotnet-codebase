using System.Collections.Generic;
using Advantage.DigitalNetwork.Authorization.Users.Dto;
using Advantage.DigitalNetwork.Dto;

namespace Advantage.DigitalNetwork.Authorization.Users.Exporting
{
    public interface IUserListExcelExporter
    {
        FileDto ExportToFile(List<UserListDto> userListDtos);
    }
}