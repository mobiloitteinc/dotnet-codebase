using System.Data.Common;
using Microsoft.EntityFrameworkCore;

namespace Advantage.DigitalNetwork.EntityFrameworkCore
{
    public static class DigitalNetworkDbContextConfigurer
    {
        public static void Configure(DbContextOptionsBuilder<DigitalNetworkDbContext> builder, string connectionString)
        {
            builder.UseSqlServer(connectionString);
        }

        public static void Configure(DbContextOptionsBuilder<DigitalNetworkDbContext> builder, DbConnection connection)
        {
            builder.UseSqlServer(connection);
        }
    }
}