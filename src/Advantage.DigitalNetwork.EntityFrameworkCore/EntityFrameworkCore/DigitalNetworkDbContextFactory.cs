﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;
using Advantage.DigitalNetwork.Configuration;
using Advantage.DigitalNetwork.Web;

namespace Advantage.DigitalNetwork.EntityFrameworkCore
{
    /* This class is needed to run "dotnet ef ..." commands from command line on development. Not used anywhere else */
    public class DigitalNetworkDbContextFactory : IDesignTimeDbContextFactory<DigitalNetworkDbContext>
    {
        public DigitalNetworkDbContext CreateDbContext(string[] args)
        {
            var builder = new DbContextOptionsBuilder<DigitalNetworkDbContext>();
            var configuration = AppConfigurations.Get(WebContentDirectoryFinder.CalculateContentRootFolder(), addUserSecrets: true);

            DigitalNetworkDbContextConfigurer.Configure(builder, configuration.GetConnectionString(DigitalNetworkConsts.ConnectionStringName));

            return new DigitalNetworkDbContext(builder.Options);
        }
    }
}