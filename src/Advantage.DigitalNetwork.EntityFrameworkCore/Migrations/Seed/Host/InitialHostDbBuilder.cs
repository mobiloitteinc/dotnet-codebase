﻿using Advantage.DigitalNetwork.EntityFrameworkCore;

namespace Advantage.DigitalNetwork.Migrations.Seed.Host
{
    public class InitialHostDbBuilder
    {
        private readonly DigitalNetworkDbContext _context;

        public InitialHostDbBuilder(DigitalNetworkDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            new DefaultEditionCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();

            _context.SaveChanges();
        }
    }
}
