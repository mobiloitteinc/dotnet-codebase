﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Advantage.DigitalNetwork.Web.Public.Views
{
    public abstract class DigitalNetworkViewComponent : AbpViewComponent
    {
        protected DigitalNetworkViewComponent()
        {
            LocalizationSourceName = DigitalNetworkConsts.LocalizationSourceName;
        }
    }
}