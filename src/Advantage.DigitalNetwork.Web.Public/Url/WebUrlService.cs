﻿using Abp.Dependency;
using Advantage.DigitalNetwork.Configuration;
using Advantage.DigitalNetwork.Url;
using Advantage.DigitalNetwork.Web.Url;

namespace Advantage.DigitalNetwork.Web.Public.Url
{
    public class WebUrlService : WebUrlServiceBase, IWebUrlService, ITransientDependency
    {
        public WebUrlService(
            IAppConfigurationAccessor appConfigurationAccessor) :
            base(appConfigurationAccessor)
        {
        }

        public override string WebSiteRootAddressFormatKey => "App:WebSiteRootAddress";

        public override string ServerRootAddressFormatKey => "App:AdminWebSiteRootAddress";
    }
}