﻿using Microsoft.AspNetCore.Mvc;
using Advantage.DigitalNetwork.Web.Controllers;

namespace Advantage.DigitalNetwork.Web.Public.Controllers
{
    public class AboutController : DigitalNetworkControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}