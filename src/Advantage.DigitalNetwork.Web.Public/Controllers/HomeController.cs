using Microsoft.AspNetCore.Mvc;
using Advantage.DigitalNetwork.Web.Controllers;

namespace Advantage.DigitalNetwork.Web.Public.Controllers
{
    public class HomeController : DigitalNetworkControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}