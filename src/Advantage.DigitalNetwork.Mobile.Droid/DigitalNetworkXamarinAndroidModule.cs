﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Advantage.DigitalNetwork
{
    [DependsOn(typeof(DigitalNetworkXamarinSharedModule))]
    public class DigitalNetworkXamarinAndroidModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DigitalNetworkXamarinAndroidModule).GetAssembly());
        }
    }
}