﻿using Microsoft.AspNetCore.Antiforgery;

namespace Advantage.DigitalNetwork.Web.Controllers
{
    public class AntiForgeryController : DigitalNetworkControllerBase
    {
        private readonly IAntiforgery _antiforgery;

        public AntiForgeryController(IAntiforgery antiforgery)
        {
            _antiforgery = antiforgery;
        }

        public void GetToken()
        {
            _antiforgery.SetCookieTokenAndHeader(HttpContext);
        }
    }
}
