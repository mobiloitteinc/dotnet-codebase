﻿using Abp.Auditing;
using Microsoft.AspNetCore.Mvc;

namespace Advantage.DigitalNetwork.Web.Controllers
{
    public class HomeController : DigitalNetworkControllerBase
    {
        [DisableAuditing]
        public IActionResult Index()
        {
            return Redirect("/swagger");
        }
    }
}
