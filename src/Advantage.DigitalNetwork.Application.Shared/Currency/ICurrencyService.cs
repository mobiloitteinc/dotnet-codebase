﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Advantage.DigitalNetwork.Currency.Dto;
using System.Collections.Generic;

namespace Advantage.DigitalNetwork.Currency
{
    public interface ICurrencyService : IApplicationService
    {
        ListResultDto<CurrencyDto> GetCurrencies();
    }
}
