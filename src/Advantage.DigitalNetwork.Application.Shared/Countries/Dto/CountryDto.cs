﻿using Abp.Application.Services.Dto;
using System;
using System.ComponentModel.DataAnnotations;

namespace Advantage.DigitalNetwork.Countries.Dto
{
    public class CountryDto: EntityDto
    {
        //public DateTime CreationTime { get; set; }
       // public int Id { get; set; }
        [Required]
        [StringLength(30)]
        public string Name { get; set; }
        [Required]
        [StringLength(30)]
        public bool AllowsBilling { get; set; }
        [Required]
        [StringLength(30)]
        public bool AllowsShipping { get; set; }
        [Required]
        [StringLength(30)]
        public string TwoLetterIsoCode { get; set; }
        [Required]
        [StringLength(30)]
        public string ThreeLetterIsoCode { get; set; }
        [Required]
        [StringLength(30)]
        public int NumericIsoCode { get; set; }
        [StringLength(30)]
        public bool SubjectToVat { get; set; }
        [StringLength(30)]
        public bool Published { get; set; }
        public int DisplayOrder { get; set; }
        public DateTime CreationTime { get; set; }
        public bool LimitedToStores { get; set; }
    }

}
