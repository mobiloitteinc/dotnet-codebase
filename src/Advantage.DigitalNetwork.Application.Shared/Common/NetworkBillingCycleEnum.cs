﻿namespace Advantage.DigitalNetwork.Configuration.Host.Dto
{
    public enum NetworkBillingCycleEnum
    {
        Daily = 10,
        LastDayOfTheWeek = 20,
        LastDayOfTheWeekBiweekly = 30,
        LastDayOfTheMonth = 40,
        Quaterly = 50,
        SemiAnnually = 60,
        Annually = 70,
        Custom = 100
    }
}