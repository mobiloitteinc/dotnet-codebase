﻿using Abp.Application.Services;
using Advantage.DigitalNetwork.Dto;
using Advantage.DigitalNetwork.Logging.Dto;

namespace Advantage.DigitalNetwork.Logging
{
    public interface IWebLogAppService : IApplicationService
    {
        GetLatestWebLogsOutput GetLatestWebLogs();

        FileDto DownloadWebLogs();
    }
}
