﻿using Advantage.DigitalNetwork.Dto;

namespace Advantage.DigitalNetwork.Organizations.Dto
{
    public class FindOrganizationUnitUsersInput : PagedAndFilteredInputDto
    {
        public long OrganizationUnitId { get; set; }
    }
}
