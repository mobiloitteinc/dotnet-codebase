﻿using Abp.Configuration;

namespace Advantage.DigitalNetwork.Timing.Dto
{
    public class GetTimezonesInput
    {
        public SettingScopes DefaultTimezoneScope { get; set; }
    }
}
