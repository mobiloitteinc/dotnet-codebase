﻿using Abp.Application.Services;
using Abp.Application.Services.Dto;
using Advantage.DigitalNetwork.Tax.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Advantage.DigitalNetwork.Tax
{
    public interface ITaxService : IApplicationService
    {
        ListResultDto<TaxCategoryDto> GetTaxCategories();
        //List<TaxCategoryDto> GetTaxRate();
    }
}
