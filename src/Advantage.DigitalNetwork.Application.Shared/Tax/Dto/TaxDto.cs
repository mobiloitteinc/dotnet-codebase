﻿using Abp.Application.Services.Dto;
using System;
using System.Collections.Generic;
using System.Text;

namespace Advantage.DigitalNetwork.Tax.Dto
{
    public class TaxDto : EntityDto
    {
        public int StoreId { get; set; }
        public int TaxCategoryId { get; set; }
        public int CountryId { get; set; }
        public int StateProvinceId { get; set; }
        public string Zip { get; set; }
        public decimal Percentage { get; set; }
        public DateTime CreationTime { get; set; }
    }
}
