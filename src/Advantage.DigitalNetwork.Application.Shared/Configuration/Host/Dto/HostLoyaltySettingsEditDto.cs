﻿namespace Advantage.DigitalNetwork.Configuration.Host.Dto
{
    public class HostLoyaltySettingsEditDto
    {
        public double NetworkPointValueWhenIssued { get; set; }

        public double NetworkPointValueWhenRedeemed { get; set; }

        public bool UsePercentForCalculation  { get; set; }

        public NetworkBillingCycleEnum BillingCycle { get; set; }

        public int MyProperty { get; set; }
    }
}