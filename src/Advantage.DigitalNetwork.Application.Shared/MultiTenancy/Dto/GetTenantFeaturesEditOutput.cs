using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Advantage.DigitalNetwork.Editions.Dto;

namespace Advantage.DigitalNetwork.MultiTenancy.Dto
{
    public class GetTenantFeaturesEditOutput
    {
        public List<NameValueDto> FeatureValues { get; set; }

        public List<FlatFeatureDto> Features { get; set; }
    }
}