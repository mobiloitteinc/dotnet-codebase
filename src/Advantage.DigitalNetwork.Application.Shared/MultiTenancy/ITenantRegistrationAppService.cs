using System.Threading.Tasks;
using Abp.Application.Services;
using Advantage.DigitalNetwork.Editions.Dto;
using Advantage.DigitalNetwork.MultiTenancy.Dto;

namespace Advantage.DigitalNetwork.MultiTenancy
{
    public interface ITenantRegistrationAppService: IApplicationService
    {
        Task<RegisterTenantOutput> RegisterTenant(RegisterTenantInput input);

        Task<EditionsSelectOutput> GetEditionsForSelect();

        Task<EditionSelectDto> GetEdition(int editionId);
    }
}