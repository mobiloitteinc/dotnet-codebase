﻿using System.ComponentModel.DataAnnotations;

namespace Advantage.DigitalNetwork.Authorization.Users.Dto
{
    public class ChangeUserLanguageDto
    {
        [Required]
        public string LanguageName { get; set; }
    }
}
