﻿using System.Collections.Generic;
using Advantage.DigitalNetwork.Authorization.Permissions.Dto;

namespace Advantage.DigitalNetwork.Authorization.Users.Dto
{
    public class GetUserPermissionsForEditOutput
    {
        public List<FlatPermissionDto> Permissions { get; set; }

        public List<string> GrantedPermissionNames { get; set; }
    }
}