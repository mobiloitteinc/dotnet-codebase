﻿using Advantage.DigitalNetwork.Security;

namespace Advantage.DigitalNetwork.Authorization.Users.Profile.Dto
{
    public class GetPasswordComplexitySettingOutput
    {
        public PasswordComplexitySetting Setting { get; set; }
    }
}
