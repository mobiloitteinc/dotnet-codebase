﻿using Advantage.DigitalNetwork.Models.Users;
using Advantage.DigitalNetwork.ViewModels;
using Xamarin.Forms;

namespace Advantage.DigitalNetwork.Views
{
    public partial class UsersView : ContentPage, IXamarinView
    {
        public UsersView()
        {
            InitializeComponent();
        }

        public async void ListView_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            await ((UsersViewModel) BindingContext).LoadMoreUserIfNeedsAsync(e.Item as UserListModel);
        }
    }
}