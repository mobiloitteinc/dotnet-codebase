﻿using Advantage.DigitalNetwork.Models.Tenants;
using Advantage.DigitalNetwork.ViewModels;
using Xamarin.Forms;

namespace Advantage.DigitalNetwork.Views
{
    public partial class TenantsView : ContentPage, IXamarinView
    {
        public TenantsView()
        {
            InitializeComponent();
        }

        private async void ListView_OnItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            await ((TenantsViewModel)BindingContext).LoadMoreTenantsIfNeedsAsync(e.Item as TenantListModel);
        }
    }
}