﻿using Abp.AutoMapper;
using Advantage.DigitalNetwork.Organizations.Dto;

namespace Advantage.DigitalNetwork.Models.Users
{
    [AutoMapFrom(typeof(OrganizationUnitDto))]
    public class OrganizationUnitModel : OrganizationUnitDto
    {
        public bool IsAssigned { get; set; }
    }
}