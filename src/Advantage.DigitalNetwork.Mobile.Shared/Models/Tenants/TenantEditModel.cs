﻿using System.ComponentModel;
using Abp.AutoMapper;
using Advantage.DigitalNetwork.MultiTenancy.Dto;

namespace Advantage.DigitalNetwork.Models.Tenants
{
    [AutoMapFrom(typeof(TenantEditDto))]
    public class TenantEditModel : TenantEditDto, INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        protected void RaisePropertyChanged(string name)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }
    }
}