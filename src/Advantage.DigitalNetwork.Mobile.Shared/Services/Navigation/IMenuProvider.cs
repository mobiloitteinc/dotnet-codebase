using System.Collections.Generic;
using MvvmHelpers;
using Advantage.DigitalNetwork.Models.NavigationMenu;

namespace Advantage.DigitalNetwork.Services.Navigation
{
    public interface IMenuProvider
    {
        ObservableRangeCollection<NavigationMenuItem> GetAuthorizedMenuItems(Dictionary<string, string> grantedPermissions);
    }
}