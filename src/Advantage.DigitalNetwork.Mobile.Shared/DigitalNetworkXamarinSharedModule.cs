﻿using Abp.AutoMapper;
using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Advantage.DigitalNetwork
{
    [DependsOn(typeof(DigitalNetworkClientModule), typeof(AbpAutoMapperModule))]
    public class DigitalNetworkXamarinSharedModule : AbpModule
    {
        public override void PreInitialize()
        {
            Configuration.Localization.IsEnabled = false;
            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DigitalNetworkXamarinSharedModule).GetAssembly());
        }
    }
}