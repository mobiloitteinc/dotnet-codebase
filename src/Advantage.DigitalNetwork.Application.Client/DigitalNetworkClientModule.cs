﻿using Abp.Modules;
using Abp.Reflection.Extensions;

namespace Advantage.DigitalNetwork
{
    public class DigitalNetworkClientModule : AbpModule
    {
        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(typeof(DigitalNetworkClientModule).GetAssembly());
        }
    }
}
