﻿(function () {
    $(function () {

        var _$taxRateTable = $('#TaxRateTable');
        //var _countryService = abp.services.app.CountryService;
        var _permissions = {
            create: abp.auth.hasPermission('Pages.Administration.TaxRate.Create'),
            edit: abp.auth.hasPermission('Pages.Administration.TaxRate.Edit'),
            'delete': abp.auth.hasPermission('Pages.Administration.TaxRate.Delete')
        };

        var _createOrEditModal = new app.ModalManager({
            viewUrl: abp.appPath + 'App/TaxRate/CreateOrEditModal',
            scriptUrl: abp.appPath + 'view-resources/Areas/App/Views/Tax/TaxRate/_CreateOrEditModal.js',
            modalClass: 'CreateOrEditUserModal'
        });
        $('#ShowAdvancedFiltersSpan').click(function () {
            $('#ShowAdvancedFiltersSpan').hide();
            $('#HideAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideDown();
        });

        $('#HideAdvancedFiltersSpan').click(function () {
            $('#HideAdvancedFiltersSpan').hide();
            $('#ShowAdvancedFiltersSpan').show();
            $('#AdvacedAuditFiltersArea').slideUp();
        });

        $('#CreateNewTaxRateButton').click(function () {
            _createOrEditModal.open();
        });
        $('#TaxRateTableFilter').focus();
    });
})();