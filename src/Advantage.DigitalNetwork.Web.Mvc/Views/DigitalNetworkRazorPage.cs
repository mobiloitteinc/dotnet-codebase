﻿using Abp.AspNetCore.Mvc.Views;
using Abp.Runtime.Session;
using Microsoft.AspNetCore.Mvc.Razor.Internal;

namespace Advantage.DigitalNetwork.Web.Views
{
    public abstract class DigitalNetworkRazorPage<TModel> : AbpRazorPage<TModel>
    {
        [RazorInject]
        public IAbpSession AbpSession { get; set; }

        protected DigitalNetworkRazorPage()
        {
            LocalizationSourceName = DigitalNetworkConsts.LocalizationSourceName;
        }
    }
}
