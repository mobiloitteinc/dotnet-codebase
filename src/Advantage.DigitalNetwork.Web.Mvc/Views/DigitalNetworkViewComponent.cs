﻿using Abp.AspNetCore.Mvc.ViewComponents;

namespace Advantage.DigitalNetwork.Web.Views
{
    public abstract class DigitalNetworkViewComponent : AbpViewComponent
    {
        protected DigitalNetworkViewComponent()
        {
            LocalizationSourceName = DigitalNetworkConsts.LocalizationSourceName;
        }
    }
}