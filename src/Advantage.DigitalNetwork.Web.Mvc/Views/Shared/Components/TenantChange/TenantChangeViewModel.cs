﻿using Abp.AutoMapper;
using Advantage.DigitalNetwork.Sessions.Dto;

namespace Advantage.DigitalNetwork.Web.Views.Shared.Components.TenantChange
{
    [AutoMapFrom(typeof(GetCurrentLoginInformationsOutput))]
    public class TenantChangeViewModel
    {
        public TenantLoginInfoDto Tenant { get; set; }
    }
}