﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Advantage.DigitalNetwork.Web.Session;

namespace Advantage.DigitalNetwork.Web.Views.Shared.Components.AccountLogo
{
    public class AccountLogoViewComponent : DigitalNetworkViewComponent
    {
        private readonly IPerRequestSessionCache _sessionCache;

        public AccountLogoViewComponent(IPerRequestSessionCache sessionCache)
        {
            _sessionCache = sessionCache;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var loginInfo = await _sessionCache.GetCurrentLoginInformationsAsync();
            return View(new AccountLogoViewModel(loginInfo));
        }
    }
}
