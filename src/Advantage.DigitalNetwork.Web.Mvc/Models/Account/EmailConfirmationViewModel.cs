using Advantage.DigitalNetwork.Authorization.Accounts.Dto;

namespace Advantage.DigitalNetwork.Web.Models.Account
{
    public class EmailConfirmationViewModel : ActivateEmailInput
    {
        /// <summary>
        /// Tenant id.
        /// </summary>
        public int? TenantId { get; set; }
    }
}