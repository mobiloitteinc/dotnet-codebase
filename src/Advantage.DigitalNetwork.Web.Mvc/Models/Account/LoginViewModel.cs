﻿namespace Advantage.DigitalNetwork.Web.Models.Account
{
    public class LoginViewModel : LoginModel
    {
        public bool RememberMe { get; set; }
    }
}