using System.ComponentModel.DataAnnotations;

namespace Advantage.DigitalNetwork.Web.Models.Account
{
    public class SendPasswordResetLinkViewModel
    {
        [Required]
        public string EmailAddress { get; set; }
    }
}