﻿using Abp.AutoMapper;
using Advantage.DigitalNetwork.MultiTenancy.Dto;

namespace Advantage.DigitalNetwork.Web.Models.TenantRegistration
{
    [AutoMapFrom(typeof(RegisterTenantOutput))]
    public class TenantRegisterResultViewModel : RegisterTenantOutput
    {
        public string TenantLoginAddress { get; set; }
    }
}