﻿using Advantage.DigitalNetwork.Editions;
using Advantage.DigitalNetwork.Editions.Dto;
using Advantage.DigitalNetwork.Security;
using Advantage.DigitalNetwork.MultiTenancy.Payments;
using Advantage.DigitalNetwork.MultiTenancy.Payments.Dto;

namespace Advantage.DigitalNetwork.Web.Models.TenantRegistration
{
    public class TenantRegisterViewModel
    {
        public PasswordComplexitySetting PasswordComplexitySetting { get; set; }

        public int? EditionId { get; set; }

        public string PaymentId { get; set; }

        public SubscriptionPaymentGatewayType? Gateway { get; set; }

        public SubscriptionStartType? SubscriptionStartType { get; set; }

        public EditionSelectDto Edition { get; set; }

        public EditionPaymentType EditionPaymentType { get; set; }

        public bool ShowPaymentExpireNotification()
        {
            return !string.IsNullOrEmpty(PaymentId);
        }
    }
}
