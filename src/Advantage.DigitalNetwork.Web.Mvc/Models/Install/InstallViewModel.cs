﻿using System.Collections.Generic;
using Abp.Localization;
using Advantage.DigitalNetwork.Install.Dto;

namespace Advantage.DigitalNetwork.Web.Models.Install
{
    public class InstallViewModel
    {
        public List<ApplicationLanguage> Languages { get; set; }

        public AppSettingsJsonDto AppSettingsJson { get; set; }
    }
}
