﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Advantage.DigitalNetwork.Web.Areas.App.Models.Layout;
using Advantage.DigitalNetwork.Web.Session;
using Advantage.DigitalNetwork.Web.Views;

namespace Advantage.DigitalNetwork.Web.Areas.App.Views.Shared.Components.AppFooter
{
    public class AppFooterViewComponent : DigitalNetworkViewComponent
    {
        private readonly IPerRequestSessionCache _sessionCache;

        public AppFooterViewComponent(IPerRequestSessionCache sessionCache)
        {
            _sessionCache = sessionCache;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            var footerModel = new FooterViewModel
            {
                LoginInformations = await _sessionCache.GetCurrentLoginInformationsAsync()
            };

            return View(footerModel);
        }
    }
}
