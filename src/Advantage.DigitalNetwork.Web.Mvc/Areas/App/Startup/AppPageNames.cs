﻿namespace Advantage.DigitalNetwork.Web.Areas.App.Startup
{
    public class AppPageNames
    {
        public static class Common
        {
            public const string Administration = "Administration";
            public const string Roles = "Administration.Roles";
            public const string Users = "Administration.Users";
            public const string AuditLogs = "Administration.AuditLogs";
            public const string OrganizationUnits = "Administration.OrganizationUnits";
            public const string Languages = "Administration.Languages";
            public const string DemoUiComponents = "Administration.DemoUiComponents";
            public const string UiCustomization = "Administration.UiCustomization";
            public const string Countries = "Administration.Country";
            public const string Currency = "Administration.Currency";
            public const string Taxes = "Administration.Taxes";
            public const string TaxCategory = "Administration.Taxes.TaxCategory";
            public const string TaxRate = "Administration.Taxes.TaxRate";
        }

        public static class Host
        {
            public const string Tenants = "Tenants";
            public const string Editions = "Editions";
            public const string Maintenance = "Administration.Maintenance";
            public const string Settings = "Administration.Settings.Host";
            public const string Dashboard = "Dashboard";
        }

        public static class Tenant
        {
            public const string Dashboard = "Dashboard.Tenant";
            public const string Settings = "Administration.Settings.Tenant";
            public const string SubscriptionManagement = "Administration.SubscriptionManagement.Tenant";
        }

        public static class Rms
        {
            public const string RmsModule = nameof(Rms);
            public const string Stores = "Rms.Stores";
            public const string FloorDesigner = "Rms.Stores.FloorDesigner";
            public const string Catalog = "Rms.Catalog";
            public const string Categories = "Rms.Catalog.Categories";
            public const string Products = "Rms.Catalog.Products";
            public const string Menus = "Rms.Catalog.Menus";

        }
        public static class Crm
        {
            public const string CrmModule = nameof(Crm);
            public const string Contacts = "Crm.Contacts";
            public const string Segmentation = "Crm.Segmentation";
            public const string Leads = "Crm.Leads";
            public const string Campaigns = "Crm.Campaigns";
            public const string Mailing = "Crm.Campaigns.Mailing";
            public const string SocialCampaign = "Crm.Campaigns.Social";

            public const string Finance = "Crm.Finance";
            public const string Dashboard = "Crm.Finance.Dashboard";
            public const string Invoicing = "Crm.Finance.Invoicing";

            public const string Contracts = "Crm.Contracts";
            public const string ContractsHome = "Crm.Contracts.ContractsHome";

        }

        public static class Loyalty
        {
            public const string LoyaltyModule = nameof(Loyalty);
            public const string LoyaltyDashboard = "Loyalty.Dashboard";
            public const string LoyaltySettings = "Loyalty.Settings";


        }
        public static class Analytics
        {
            public const string AnalyticsModule = nameof(Analytics);
            public const string AdvancedAnalytics = "Analytics.Advanced";

        }
        public static class Ecommerce
        {
            public const string EcommerceModule = nameof(Ecommerce);

        }

    }
}
