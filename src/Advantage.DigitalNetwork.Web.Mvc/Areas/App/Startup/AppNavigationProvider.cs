﻿using Abp.Application.Features;
using Abp.Application.Navigation;
using Abp.Authorization;
using Abp.Localization;
using Advantage.DigitalNetwork.Authorization;
using Advantage.DigitalNetwork.Features;

namespace Advantage.DigitalNetwork.Web.Areas.App.Startup
{
    public class AppNavigationProvider : NavigationProvider
    {
        public const string MenuName = "App";

        public override void SetNavigation(INavigationProviderContext context)
        {
            var menu = context.Manager.Menus[MenuName] = new MenuDefinition(MenuName, new FixedLocalizableString("Main Menu"));

            menu
                .AddItem(new MenuItemDefinition(
                        AppPageNames.Host.Dashboard,
                        L("Dashboard"),
                        url: "App/HostDashboard",
                        icon: "flaticon-line-graph",
                        requiredPermissionName: AppPermissions.Pages_Administration_Host_Dashboard
                    )
                ).AddItem(new MenuItemDefinition(
                    AppPageNames.Host.Tenants,
                    L("Tenants"),
                    url: "App/Tenants",
                    icon: "flaticon-list-3",
                    requiredPermissionName: AppPermissions.Pages_Tenants
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Host.Editions,
                        L("Editions"),
                        url: "App/Editions",
                        icon: "flaticon-app",
                        requiredPermissionName: AppPermissions.Pages_Editions
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Tenant.Dashboard,
                        L("Dashboard"),
                        url: "App/Dashboard",
                        icon: "flaticon-line-graph",
                        requiredPermissionName: AppPermissions.Pages_Tenant_Dashboard
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Rms.RmsModule,
                        L("Rms"),
                        url: "App/Rms",
                        icon: "icon-home",
                        featureDependency: new SimpleFeatureDependency(AppFeatures.FeatureRms)
                    //permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Rms)
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Rms.Stores,
                            L("Stores"),
                            url: "App/Stores",
                            icon: "icon-layers",
                            featureDependency: new SimpleFeatureDependency(AppFeatures.FeatureRmsStores)
                        //permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Rms_Stores)
                        //requiredPermissionName: AppPermissions.Pages_Rms_Stores
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Rms.Catalog,
                            L("Catalog"),
                            url: "App/Catalog",
                            icon: "icon-layers",
                            featureDependency: new SimpleFeatureDependency(AppFeatures.FeatureRmsCatalog)
                        //permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Rms_Stores)
                        //requiredPermissionName: AppPermissions.Pages_Rms_Stores
                        ).AddItem(new MenuItemDefinition(
                                AppPageNames.Rms.Categories,
                                L("Categories"),
                                url: "App/Categories",
                                icon: "icon-layers",
                                featureDependency: new SimpleFeatureDependency(AppFeatures.FeatureRmsCatalogCategories),
                                permissionDependency: new SimplePermissionDependency(AppPermissions.Rms_Catalog)
                            //requiredPermissionName: AppPermissions.Pages_Rms_Stores
                            )
                        ).AddItem(new MenuItemDefinition(
                                AppPageNames.Rms.Products,
                                L("Products"),
                                url: "App/Products",
                                icon: "icon-layers",
                                featureDependency: new SimpleFeatureDependency(AppFeatures.FeatureRmsCatalogProducts),
                                permissionDependency: new SimplePermissionDependency(AppPermissions.Rms_Catalog)
                            //requiredPermissionName: AppPermissions.Pages_Rms_Stores
                            )
                        )
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Crm.CrmModule,
                        L("Crm"),
                        url: "App/Crm",
                        icon: "icon-home",
                        featureDependency: new SimpleFeatureDependency(AppFeatures.FeatureCrm)
                    //permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Crm)
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Crm.Contacts,
                            L("Contacts"),
                            url: "App/Contacts",
                            icon: "icon-layers",
                            featureDependency: new SimpleFeatureDependency(AppFeatures.FeatureCrmContacts)
                        //permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Crm_Contacts)
                        //requiredPermissionName: AppPermissions.Pages_Rms_Stores
                        )
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Loyalty.LoyaltyModule,
                        L("Loaylty Management"),
                        url: "App/Loyalty",
                        icon: "icon-home",
                        featureDependency: new SimpleFeatureDependency(AppFeatures.FeatureLoyalty)
                    //permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Loyalty)
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Ecommerce.EcommerceModule,
                        L("ECommerce"),
                        url: "App/eCommerce",
                        icon: "icon-home",
                        featureDependency: new SimpleFeatureDependency(AppFeatures.FeatureECommerce)
                    //permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_ECommerce)
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Analytics.AnalyticsModule,
                        L("Analytics"),
                        url: "App/Analytics",
                        icon: "icon-home",
                        featureDependency: new SimpleFeatureDependency(AppFeatures.FeatureAnalytics)
                    //permissionDependency: new SimplePermissionDependency(AppPermissions.Pages_Anaytics)
                    )
                ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.Administration,
                        L("Administration"),
                        icon: "flaticon-interface-8"
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.OrganizationUnits,
                            L("OrganizationUnits"),
                            url: "App/OrganizationUnits",
                            icon: "flaticon-map",
                            requiredPermissionName: AppPermissions.Pages_Administration_OrganizationUnits
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Roles,
                            L("Roles"),
                            url: "App/Roles",
                            icon: "flaticon-suitcase",
                            requiredPermissionName: AppPermissions.Pages_Administration_Roles
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Users,
                            L("Users"),
                            url: "App/Users",
                            icon: "flaticon-users",
                            requiredPermissionName: AppPermissions.Pages_Administration_Users
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Languages,
                            L("Languages"),
                            url: "App/Languages",
                            icon: "flaticon-tabs",
                            requiredPermissionName: AppPermissions.Pages_Administration_Languages
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.AuditLogs,
                            L("AuditLogs"),
                            url: "App/AuditLogs",
                            icon: "flaticon-folder-1",
                            requiredPermissionName: AppPermissions.Pages_Administration_AuditLogs
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Host.Maintenance,
                            L("Maintenance"),
                            url: "App/Maintenance",
                            icon: "flaticon-lock",
                            requiredPermissionName: AppPermissions.Pages_Administration_Host_Maintenance
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.SubscriptionManagement,
                            L("Subscription"),
                            url: "App/SubscriptionManagement",
                            icon: "flaticon-refresh"
                            ,
                            requiredPermissionName: AppPermissions.Pages_Administration_Tenant_SubscriptionManagement
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.UiCustomization,
                            L("VisualSettings"),
                            url: "App/UiCustomization",
                            icon: "flaticon-medical",
                            requiredPermissionName: AppPermissions.Pages_Administration_UiCustomization
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Host.Settings,
                            L("Settings"),
                            url: "App/HostSettings",
                            icon: "flaticon-settings",
                            requiredPermissionName: AppPermissions.Pages_Administration_Host_Settings
                        )
                    )
                    .AddItem(new MenuItemDefinition(
                            AppPageNames.Tenant.Settings,
                            L("Settings"),
                            url: "App/Settings",
                            icon: "flaticon-settings",
                            requiredPermissionName: AppPermissions.Pages_Administration_Tenant_Settings
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Currency,
                            L("Currencies"),
                            url: "App/Currencies",
                            icon: "flaticon-folder-1",
                            requiredPermissionName: AppPermissions.Pages_Administration_OrganizationUnits
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Countries,
                            L("Countries"),
                            url: "App/Countries",
                            icon: "flaticon-folder-1",
                            requiredPermissionName: AppPermissions.Pages_Administration_OrganizationUnits
                        )
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.Taxes,
                            L("Tax"),
                            url: "App/Tax",
                            icon: "flaticon-folder-1",
                            requiredPermissionName: AppPermissions.Pages_Administration_OrganizationUnits
                    ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.TaxCategory,
                            L("TaxCategory"),
                            url: "App/TaxCategory",
                            icon: "flaticon-folder-1",
                            requiredPermissionName: AppPermissions.Pages_Administration_OrganizationUnits
                           )
                     ).AddItem(new MenuItemDefinition(
                            AppPageNames.Common.TaxRate,
                            L("TaxRate"),
                            url: "App/TaxRate",
                            icon: "flaticon-folder-1",
                            requiredPermissionName: AppPermissions.Pages_Administration_OrganizationUnits
                           )
                           )
                     ).AddItem(new MenuItemDefinition(
                        AppPageNames.Common.DemoUiComponents,
                        L("DemoUiComponents"),
                        url: "App/DemoUiComponents",
                        icon: "flaticon-shapes",
                        requiredPermissionName: AppPermissions.Pages_DemoUiComponents
                    )
                )
                );
        }
        private static ILocalizableString L(string name)
        {
            return new LocalizableString(name, DigitalNetworkConsts.LocalizationSourceName);
        }
    }
}