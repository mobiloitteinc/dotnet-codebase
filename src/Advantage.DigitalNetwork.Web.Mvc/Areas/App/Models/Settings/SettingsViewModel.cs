﻿using System.Collections.Generic;
using Abp.Application.Services.Dto;
using Advantage.DigitalNetwork.Configuration.Tenants.Dto;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Settings
{
    public class SettingsViewModel
    {
        public TenantSettingsEditDto Settings { get; set; }
        
        public List<ComboboxItemDto> TimezoneItems { get; set; }
    }
}