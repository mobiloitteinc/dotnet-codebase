using Abp.AutoMapper;
using Advantage.DigitalNetwork.Editions;
using Advantage.DigitalNetwork.MultiTenancy.Payments.Dto;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.SubscriptionManagement
{
    [AutoMapTo(typeof(ExecutePaymentDto))]
    public class PaymentResultViewModel : SubscriptionPaymentDto
    {
        public EditionPaymentType EditionPaymentType { get; set; }
    }
}