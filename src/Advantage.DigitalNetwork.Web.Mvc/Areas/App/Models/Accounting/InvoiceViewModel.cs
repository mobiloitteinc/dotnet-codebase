﻿using Advantage.DigitalNetwork.MultiTenancy.Accounting.Dto;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Accounting
{
    public class InvoiceViewModel
    {
        public InvoiceDto Invoice { get; set; }
    }
}
