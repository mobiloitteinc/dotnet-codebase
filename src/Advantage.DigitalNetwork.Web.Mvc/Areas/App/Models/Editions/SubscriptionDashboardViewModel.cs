﻿using Advantage.DigitalNetwork.Sessions.Dto;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Editions
{
    public class SubscriptionDashboardViewModel
    {
        public GetCurrentLoginInformationsOutput LoginInformations { get; set; }
    }
}
