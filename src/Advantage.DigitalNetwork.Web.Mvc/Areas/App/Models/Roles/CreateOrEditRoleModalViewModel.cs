﻿using Abp.AutoMapper;
using Advantage.DigitalNetwork.Authorization.Roles.Dto;
using Advantage.DigitalNetwork.Web.Areas.App.Models.Common;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Roles
{
    [AutoMapFrom(typeof(GetRoleForEditOutput))]
    public class CreateOrEditRoleModalViewModel : GetRoleForEditOutput, IPermissionsEditViewModel
    {
        public bool IsEditMode
        {
            get { return Role.Id.HasValue; }
        }

        public CreateOrEditRoleModalViewModel(GetRoleForEditOutput output)
        {
            output.MapTo(this);
        }
    }
}