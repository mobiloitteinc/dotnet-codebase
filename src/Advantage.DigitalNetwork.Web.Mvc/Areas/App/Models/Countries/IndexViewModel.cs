﻿using Abp.Application.Services.Dto;
using Abp.AutoMapper;
using Advantage.DigitalNetwork.Countries.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Countries
{
    [AutoMapFrom(typeof(ListResultDto<CountryDto>))]
    public class IndexViewModel: ListResultDto<CountryDto>
    {
        public IndexViewModel(ListResultDto<CountryDto> output)
        {
            output.MapTo(this);
        }
    }
}
