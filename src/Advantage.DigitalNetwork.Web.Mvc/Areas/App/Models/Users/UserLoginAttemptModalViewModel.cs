using System.Collections.Generic;
using Advantage.DigitalNetwork.Authorization.Users.Dto;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Users
{
    public class UserLoginAttemptModalViewModel
    {
        public List<UserLoginAttemptDto> LoginAttempts { get; set; }
    }
}