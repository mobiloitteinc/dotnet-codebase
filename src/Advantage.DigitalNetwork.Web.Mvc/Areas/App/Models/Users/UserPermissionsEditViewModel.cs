using Abp.AutoMapper;
using Advantage.DigitalNetwork.Authorization.Users;
using Advantage.DigitalNetwork.Authorization.Users.Dto;
using Advantage.DigitalNetwork.Web.Areas.App.Models.Common;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Users
{
    [AutoMapFrom(typeof(GetUserPermissionsForEditOutput))]
    public class UserPermissionsEditViewModel : GetUserPermissionsForEditOutput, IPermissionsEditViewModel
    {
        public User User { get; private set; }

        public UserPermissionsEditViewModel(GetUserPermissionsForEditOutput output, User user)
        {
            User = user;
            output.MapTo(this);
        }
    }
}