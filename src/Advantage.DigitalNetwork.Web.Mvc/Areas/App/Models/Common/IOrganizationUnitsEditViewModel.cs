using System.Collections.Generic;
using Advantage.DigitalNetwork.Organizations.Dto;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Common
{
    public interface IOrganizationUnitsEditViewModel
    {
        List<OrganizationUnitDto> AllOrganizationUnits { get; set; }

        List<string> MemberedOrganizationUnits { get; set; }
    }
}