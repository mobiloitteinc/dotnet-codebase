using System.Collections.Generic;
using Advantage.DigitalNetwork.Authorization.Permissions.Dto;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Common
{
    public interface IPermissionsEditViewModel
    {
        List<FlatPermissionDto> Permissions { get; set; }

        List<string> GrantedPermissionNames { get; set; }
    }
}