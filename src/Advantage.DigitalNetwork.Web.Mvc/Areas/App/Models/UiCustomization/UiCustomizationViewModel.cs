﻿using Advantage.DigitalNetwork.Configuration.Dto;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.UiCustomization
{
    public class UiCustomizationViewModel
    {
        public UiCustomizationSettingsEditDto Settings { get; set; }

        public bool HasUiCustomizationPagePermission { get; set; }
    }
}
