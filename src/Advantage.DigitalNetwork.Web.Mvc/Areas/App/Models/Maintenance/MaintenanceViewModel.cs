﻿using System.Collections.Generic;
using Advantage.DigitalNetwork.Caching.Dto;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Maintenance
{
    public class MaintenanceViewModel
    {
        public IReadOnlyList<CacheDto> Caches { get; set; }
    }
}