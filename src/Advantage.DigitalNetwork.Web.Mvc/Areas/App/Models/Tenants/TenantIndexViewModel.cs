﻿using System.Collections.Generic;
using Advantage.DigitalNetwork.Editions.Dto;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Tenants
{
    public class TenantIndexViewModel
    {
        public List<SubscribableEditionComboboxItemDto> EditionItems { get; set; }
    }
}