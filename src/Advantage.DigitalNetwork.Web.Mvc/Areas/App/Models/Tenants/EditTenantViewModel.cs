﻿using System.Collections.Generic;
using Advantage.DigitalNetwork.Editions.Dto;
using Advantage.DigitalNetwork.MultiTenancy.Dto;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Tenants
{
    public class EditTenantViewModel
    {
        public TenantEditDto Tenant { get; set; }

        public IReadOnlyList<SubscribableEditionComboboxItemDto> EditionItems { get; set; }

        public EditTenantViewModel(TenantEditDto tenant, IReadOnlyList<SubscribableEditionComboboxItemDto> editionItems)
        {
            Tenant = tenant;
            EditionItems = editionItems;
        }
    }
}