using Abp.AutoMapper;
using Advantage.DigitalNetwork.MultiTenancy;
using Advantage.DigitalNetwork.MultiTenancy.Dto;
using Advantage.DigitalNetwork.Web.Areas.App.Models.Common;

namespace Advantage.DigitalNetwork.Web.Areas.App.Models.Tenants
{
    [AutoMapFrom(typeof (GetTenantFeaturesEditOutput))]
    public class TenantFeaturesEditViewModel : GetTenantFeaturesEditOutput, IFeatureEditViewModel
    {
        public Tenant Tenant { get; set; }

        public TenantFeaturesEditViewModel(Tenant tenant, GetTenantFeaturesEditOutput output)
        {
            Tenant = tenant;
            output.MapTo(this);
        }
    }
}