﻿using Abp.AspNetCore.Mvc.Authorization;
using Advantage.DigitalNetwork.Authorization;
using Advantage.DigitalNetwork.Currency;
using Advantage.DigitalNetwork.Web.Areas.App.Models.Countries;
using Advantage.DigitalNetwork.Web.Areas.App.Models.Currencies;
using Advantage.DigitalNetwork.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace Advantage.DigitalNetwork.Web.Mvc.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_OrganizationUnits)]
    public class CurrenciesController : DigitalNetworkControllerBase
    {
        private readonly ICurrencyService _currencyService;
        public CurrenciesController(
            ICurrencyService currenciesService
            )
        {
            _currencyService = currenciesService;
        }
        public IActionResult Index()
        {
            var sss = _currencyService.GetCurrencies();
            return View();
        }
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
            // var output = await _userAppService.GetUserForEdit(new NullableIdDto<long> { Id = id });
            var viewModel = new CreateOrEditCurrencyViewModel();
            //var viewModel = new CreateOrEditCountryModalViewModel();
            return PartialView("_CreateOrEditCurrencyModel", viewModel);
        }

    }
}