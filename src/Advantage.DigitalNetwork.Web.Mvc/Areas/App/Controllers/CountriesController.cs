﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using Advantage.DigitalNetwork.Authorization;
using Advantage.DigitalNetwork.Web.Controllers;
using Abp.Domain.Repositories;
using Advantage.DigitalNetwork.Country;
using Advantage.DigitalNetwork.Web.Areas.App.Models.Countries;
using Advantage.DigitalNetwork.Authorization.Permissions;
using Advantage.DigitalNetwork.Countries.Dto;
using Advantage.DigitalNetwork.Countries;

namespace Advantage.DigitalNetwork.Web.Mvc.Areas.App.Controllers
{
    [Area("App")]
    //[AbpMvcAuthorize(AppPermissions.Pages_Administration_OrganizationUnits)]
    public class CountriesController : DigitalNetworkControllerBase
    {
        private readonly ICountryService _countryService;
        public CountriesController(
             ICountryService countryService
             )
        {
            _countryService = countryService;
        }
        public IActionResult Index()
        {
          var sss=  _countryService.GetCountries();
            return View();
        }
        //[AbpMvcAuthorize(AppPermissions.Pages_Administration_Country_Create, AppPermissions.Pages_Administration_Country_Edit)]
        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
           // var output = await _userAppService.GetUserForEdit(new NullableIdDto<long> { Id = id });
            var viewModel = new CreateOrEditCountryModalViewModel();
            return PartialView("_CreateOrEditModal", viewModel);
        }

    }
}