﻿using Abp.AspNetCore.Mvc.Authorization;
using Microsoft.AspNetCore.Mvc;
using Advantage.DigitalNetwork.Authorization;
using Advantage.DigitalNetwork.Web.Areas.App.Models.HostDashboard;
using Advantage.DigitalNetwork.Web.Controllers;

namespace Advantage.DigitalNetwork.Web.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_Host_Dashboard)]
    public class HostDashboardController : DigitalNetworkControllerBase
    {
        private const int DashboardOnLoadReportDayCount = 7;

        public ActionResult Index()
        {
            return View(new HostDashboardViewModel(DashboardOnLoadReportDayCount));
        }
    }
}