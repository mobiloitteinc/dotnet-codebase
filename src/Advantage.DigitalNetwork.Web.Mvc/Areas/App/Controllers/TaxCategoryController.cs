﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Abp.AspNetCore.Mvc.Authorization;
using Advantage.DigitalNetwork.Authorization;
using Advantage.DigitalNetwork.Web.Controllers;
using Advantage.DigitalNetwork.Tax;
using Advantage.DigitalNetwork.Web.Areas.App.Models.Tax.TaxCategory;

namespace Advantage.DigitalNetwork.Web.Mvc.Areas.App.Controllers
{
    [Area("App")]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_OrganizationUnits)]
    public class TaxCategoryController : DigitalNetworkControllerBase
    {
        private readonly ITaxService _taxServices;
        public IActionResult Index()
        {
            //var result = _taxServices.GetTaxCategories();
            //var model = new TaxCategoryViewModel
            //{
            //    TaxCategories = result
            //};
            return View();
        }

        public async Task<PartialViewResult> CreateOrEditModal(long? id)
        {
            // var output = await _userAppService.GetUserForEdit(new NullableIdDto<long> { Id = id });
            var viewModel = new CreateOrEditTaxCategoryModalViewModel();
            return PartialView("_CreateOrEditTaxcategoryModal", viewModel);
        }
    }
}