﻿using Abp.AspNetCore.Mvc.Authorization;
using Abp.Auditing;
using Microsoft.AspNetCore.Mvc;
using Advantage.DigitalNetwork.Authorization;
using Advantage.DigitalNetwork.Web.Controllers;

namespace Advantage.DigitalNetwork.Web.Areas.App.Controllers
{
    [Area("App")]
    [DisableAuditing]
    [AbpMvcAuthorize(AppPermissions.Pages_Administration_AuditLogs)]
    public class AuditLogsController : DigitalNetworkControllerBase
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}