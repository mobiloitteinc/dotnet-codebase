﻿using Advantage.DigitalNetwork.Auditing;
using Shouldly;
using Xunit;

namespace Advantage.DigitalNetwork.Tests.Auditing
{
    public class NamespaceStripper_Tests: AppTestBase
    {
        private readonly INamespaceStripper _namespaceStripper;

        public NamespaceStripper_Tests()
        {
            _namespaceStripper = Resolve<INamespaceStripper>();
        }

        [Fact]
        public void Should_Stripe_Namespace()
        {
            var controllerName = _namespaceStripper.StripNameSpace("Advantage.DigitalNetwork.Web.Controllers.HomeController");
            controllerName.ShouldBe("HomeController");
        }

        [Theory]
        [InlineData("Advantage.DigitalNetwork.Auditing.GenericEntityService`1[[Advantage.DigitalNetwork.Storage.BinaryObject, Advantage.DigitalNetwork.Core, Version=1.10.1.0, Culture=neutral, PublicKeyToken=null]]", "GenericEntityService<BinaryObject>")]
        [InlineData("CompanyName.ProductName.Services.Base.EntityService`6[[CompanyName.ProductName.Entity.Book, CompanyName.ProductName.Core, Version=1.10.1.0, Culture=neutral, PublicKeyToken=null],[CompanyName.ProductName.Services.Dto.Book.CreateInput, N...", "EntityService<Book, CreateInput>")]
        [InlineData("Advantage.DigitalNetwork.Auditing.XEntityService`1[Advantage.DigitalNetwork.Auditing.AService`5[[Advantage.DigitalNetwork.Storage.BinaryObject, Advantage.DigitalNetwork.Core, Version=1.10.1.0, Culture=neutral, PublicKeyToken=null],[Advantage.DigitalNetwork.Storage.TestObject, Advantage.DigitalNetwork.Core, Version=1.10.1.0, Culture=neutral, PublicKeyToken=null],]]", "XEntityService<AService<BinaryObject, TestObject>>")]
        public void Should_Stripe_Generic_Namespace(string serviceName, string result)
        {
            var genericServiceName = _namespaceStripper.StripNameSpace(serviceName);
            genericServiceName.ShouldBe(result);
        }
    }
}
