﻿using Abp.Dependency;
using Abp.Reflection.Extensions;
using Microsoft.Extensions.Configuration;
using Advantage.DigitalNetwork.Configuration;

namespace Advantage.DigitalNetwork.Tests.Configuration
{
    public class TestAppConfigurationAccessor : IAppConfigurationAccessor, ISingletonDependency
    {
        public IConfigurationRoot Configuration { get; }

        public TestAppConfigurationAccessor()
        {
            Configuration = AppConfigurations.Get(
                typeof(DigitalNetworkTestModule).GetAssembly().GetDirectoryPathOrNull()
            );
        }
    }
}
